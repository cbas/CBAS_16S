library(vegan)
library(reshape2)
library(RColorBrewer)
library(ggplot2)
library(alluvial)

#if needed set the wd to the folder containing this script
#setwd("your_path_here")

bact_matrix<-read.csv("../Data/all.otutab.csv", head=T, sep="\t", row.names=1)

#select_OTUs<-names(which(rowSums(bact_matrix)/sum(rowSums(bact_matrix)) > 0.01))

#remove col 5 and 6 due to low counts. Remove cols 15-18 due to unclear position in sponge.
#bact_matrix<-bact_matrix[,-c(5,6,13,15:18)]

#OTUs 1-6 have over 5% reads, represent 70% of the total sequenced reads
#and have 100% identity to members of the core microbiome. #OTU_9 is not 100% identical but close to an alpha proteo in the core microbiome
#if included, 75% of the reads are covered.

select_OTUs<-c("OTU_1","OTU_2","OTU_3","OTU_4","OTU_5","OTU_6", "OTU_7","OTU_8","OTU_9")

bact_matrix<-bact_matrix[rownames(bact_matrix) %in% select_OTUs,]
#bact_matrix$OTUs <- rownames(bact_matrix)
#rownames(bact_matrix) <- NULL

colnames(bact_matrix) <- labels_centroid_for_groups<-c("B","B","B","B",
                                                       "C","C","B","B",
                                                       "B","B","B","B",
                                                       "B","B","C","C",
                                                       "C","C","C","B",
                                                       "B","B","B","C",
                                                       "C","C","C","C",
                                                       "C","C","C","C",
                                                       "W9","W3","W6","W9",
                                                       "B","W3","W6","C")
#bact_matrix


control <- c(15:18,19,24:32,40)#includes SoS samples = 15:18
bleached <- c(1:4, 11:14, 20:23, 37)#includes BoS samples = 11:14
shaded <- 7:10

##alluvial plot
core_matrix_melted<-melt(t(bact_matrix[,c(control,shaded,bleached)]), varnames = c("Sample","OTU"))

core_matrix_melted$Treatment<-rep(c(rep(c("C","B"), c(length(control),(length(shaded)+length(bleached))))),length(select_OTUs))

core_matrix_aggregated<-aggregate(value~Treatment+OTU, data=core_matrix_melted, sum)
core_matrix_aggregated_ctrl<-subset(core_matrix_aggregated, Treatment=="C")
core_matrix_aggregated_bleached<-subset(core_matrix_aggregated, Treatment=="B")

core_matrix_aggregated_ctrl<-cbind(core_matrix_aggregated_ctrl, core_matrix_aggregated_ctrl$value/sum(core_matrix_aggregated_ctrl$value))
colnames(core_matrix_aggregated_ctrl)<-c("Treatment","OTU","value","Freq")

core_matrix_aggregated_bleached<-cbind(core_matrix_aggregated_bleached, core_matrix_aggregated_bleached$value/sum(core_matrix_aggregated_bleached$value))
colnames(core_matrix_aggregated_bleached)<-c("Treatment","OTU","value","Freq")

core_matrix_aggregated<-rbind(core_matrix_aggregated_ctrl,core_matrix_aggregated_bleached)

svg(filename = "../Plots/otu_freq_alluvial.svg")
alluvial(core_matrix_aggregated[,c(2,1)], freq=core_matrix_aggregated$Freq, col=ifelse(core_matrix_aggregated$Treatment == "C",  "slateblue1", "lightskyblue"), layer=core_matrix_aggregated$Treatment=="B", alpha=0.6)
dev.off()
